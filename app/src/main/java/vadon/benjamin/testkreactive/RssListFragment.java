package vadon.benjamin.testkreactive;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pkmmte.pkrss.Article;
import com.pkmmte.pkrss.Callback;
import com.pkmmte.pkrss.PkRSS;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ben on 05/11/14.
 */
public class RssListFragment extends Fragment implements Callback {
    private RecyclerView rssList;
    private String baseUrl = "http://techdissected.com/feed/";
    private SwipeRefreshLayout swipeToRefresh;
    private View emptyView;

    public RssListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
        initViewItems(rootView);
        return rootView;
    }

    private void initViewItems(View rootView) {
        initEmptyView(rootView);
        initSwipeToRefresh(rootView);
        initRssList(rootView);
    }

    private void initEmptyView(View rootView) {
        emptyView = rootView.findViewById(R.id.emptyView);
    }

    private void initSwipeToRefresh(View rootView) {
        swipeToRefresh = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                PkRSS.with(getActivity()).load(baseUrl).callback(RssListFragment.this).async();
            }
        });
    }

    private void initRssList(View rootView) {
        rssList = (RecyclerView)rootView.findViewById(R.id.rssList);
        rssList.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void OnPreLoad() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                emptyView.setVisibility(View.GONE);
                swipeToRefresh.setRefreshing(true);
                rssList.setAdapter(null);
            }
        });
    }

    @Override
    public void OnLoaded(final List<Article> rssItems) {
        if(rssItems==null) return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                emptyView.setVisibility(View.GONE);
                swipeToRefresh.setRefreshing(false);
                rssList.setAdapter(new RssAdapter(rssItems));
            }
        });
    }


    @Override
    public void OnLoadFailed() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                emptyView.setVisibility(View.VISIBLE);
                swipeToRefresh.setRefreshing(false);
                rssList.setAdapter(null);
            }
        });
    }


    private class RssAdapter extends RecyclerView.Adapter<RssAdapter.RssRowHolder> {
        private final List<Article> rssItems;

        public RssAdapter(List<Article> rssItems) {
            this.rssItems = rssItems;
        }

        @Override
        public RssRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View viewContainer = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list_rss, parent, false);

            RssRowHolder holder = new RssRowHolder(viewContainer);
            viewContainer.setTag(holder);
            viewContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = ((RssRowHolder)v.getTag()).getPosition();

                    showDetailFragment(rssItems.get(position));
                }
            });
            return holder;
        }

        @Override
        public void onBindViewHolder(RssRowHolder viewHolder, int position) {
            Uri imageUri = rssItems.get(position).getImage();
            Picasso.with(getActivity())
                    .load(imageUri)
                    .placeholder(R.drawable.rss_placeholder)
                    .into(viewHolder.image);

            viewHolder.title.setText(rssItems.get(position).getTitle());
        }

        @Override
        public int getItemCount() {
            return rssItems.size();
        }

        public class RssRowHolder extends RecyclerView.ViewHolder {
            public ImageView image;
            public TextView title;

            public RssRowHolder(View view) {
                super(view);

                image = (ImageView) view.findViewById(R.id.image);
                title = (TextView) view.findViewById(R.id.title);
            }
        }
    }

    private void showDetailFragment(Article article) {
        DetailFragment detailFragment = DetailFragment.newInstance(article);
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, detailFragment, DetailFragment.TAG)
                .addToBackStack(null)
                .hide(this)
                .commit();
    }
}
