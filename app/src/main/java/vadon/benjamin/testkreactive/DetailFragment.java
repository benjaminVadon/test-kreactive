package vadon.benjamin.testkreactive;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pkmmte.pkrss.Article;
import com.squareup.picasso.Picasso;

/**
 * Created by ben on 05/11/14.
 */
public class DetailFragment extends Fragment{

    private final static String KEY_article = "article";
    public static final String TAG = DetailFragment.class.getSimpleName();

    public DetailFragment() {
    }

    public static DetailFragment newInstance(Article article) {
        DetailFragment detailFragment = new DetailFragment();
        Bundle extras = new Bundle();
        extras.putParcelable(KEY_article, article);
        detailFragment.setArguments(extras);
        return detailFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
        initViewItems(rootView);
        return rootView;
    }

    private void initViewItems(View rootView) {
        Article article = getArticle();
        if(article==null) return;;

        initImage(rootView, article);
        initTexts(rootView, article);

    }

    private Article getArticle() {
        Bundle arguments =  getArguments();
        if(arguments!=null) {
            Object parcelable = arguments.getParcelable(KEY_article);
            if (parcelable != null) {
                Article article = (Article) parcelable;
                return article;
            }
        }
        return null;
    }

    private void initImage(View rootView, Article article) {
        ImageView image = (ImageView) rootView.findViewById(R.id.image);
        Picasso.with(getActivity())
                .load(article.getImage())
                .placeholder(R.drawable.rss_placeholder)
                .into(image);
    }

    private void initTexts(View rootView, Article article) {
        TextView title = (TextView) rootView.findViewById(R.id.title);
        title.setText(article.getTitle());

        TextView date = (TextView) rootView.findViewById(R.id.date);
        date.setText(getRelativeDate(article.getDate()));

        TextView description = (TextView) rootView.findViewById(R.id.description);
        description.setText(article.getDescription());
    }
    public static CharSequence getRelativeDate(long date) {
        if(date==0){
            return "";
        }else {
            return DateUtils.getRelativeTimeSpanString(date, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS, 0);
        }
    }

}
